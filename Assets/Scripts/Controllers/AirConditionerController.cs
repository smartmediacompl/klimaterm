﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.ParticleSystem;

public class AirConditionerController : MonoBehaviour
{
    //*** private SerializeFiled
    [SerializeField]
    public bool FxActive { get; set; } = true;
    //*** Public
    public Transform wing;
    public ParticleSystem air;
    public Material hotAirMaterial;
    public Material coldAirMaterial;
    public int speedRotation = 6;
    public int maxAngleX = 45;
    public int fxThreshold = 15;
    public int minAngleX = 0;
    //*** Private
    private MainModule airMainModule; 
    private Color hotAirColor;
    private Color coldAirColor;
    private bool isCold;
    private bool isOn = false;
    private int tempMinAngleX = 0;
    private float hotColdLerpValue = 0;
    private bool startChangeAir = false;
    private void Start() {

        airMainModule = air.main;
        isCold = true;
        hotAirColor =  hotAirMaterial.GetColor("_Color");
        coldAirColor =  coldAirMaterial.GetColor("_Color");
        airMainModule.startColor = coldAirColor;
    }
    private void Update() {

        WingsAndEmissionControll();
        AirColorControll();
    }

    private void AirColorControll()
    {
        airMainModule.startColor = Color.Lerp(coldAirColor, hotAirColor, hotColdLerpValue);

        if((hotColdLerpValue <= 0 || hotColdLerpValue >= 1) && !startChangeAir){
            startChangeAir = true;
            Invoke("SetColdHot",16.0f);
        }
    }

    private void WingsAndEmissionControll(){

        if(isOn || !isOn && speedRotation < 0){
            wing.Rotate(Vector3.right, speedRotation * Time.deltaTime);
            float rot = wing.localRotation.x * 100;

            if(isOn && FxActive && rot >= fxThreshold){
                tempMinAngleX = fxThreshold;
                var emission = air.emission;
                emission.enabled = true;
            }

            if (rot >= maxAngleX || rot <= tempMinAngleX) {
                speedRotation = -speedRotation;
            }
        }
    }
    public void SetOnOff(){

        if(isOn){
            speedRotation = - Math.Abs(speedRotation);
            tempMinAngleX = minAngleX;
            var emission = air.emission;
            emission.enabled = false;
        }
        isOn = !isOn;
    }
    public void SetOn(){

        isOn = true;
    }
    public void SetOff(){

        if(isOn){
            speedRotation = - Math.Abs(speedRotation);
            tempMinAngleX = minAngleX;
            var emission = air.emission;
            emission.enabled = false;
        }
        isOn = false;
    }
    public void SetColdHot(){

        StartCoroutine(ChangeAir(.01f));
        isCold = !isCold;
    }
    private IEnumerator ChangeAir(float delayTime){

        if(hotColdLerpValue <= 0){
            for( ; hotColdLerpValue < 1; hotColdLerpValue += 0.01f){
                yield return new WaitForSeconds(delayTime);
            }
        }else if(hotColdLerpValue >= 0 ){
            for( ; hotColdLerpValue > 0; hotColdLerpValue -= 0.01f){
                yield return new WaitForSeconds(delayTime);
            }
        }
        startChangeAir = false;   
    }
    public void SetLowSpeed(){
        airMainModule.simulationSpeed = 0.3f;
    }
    public void SetMediumSpeed(){
        airMainModule.simulationSpeed = 0.4f;
    }
    public void SetHighSpeed(){
        airMainModule.simulationSpeed = 0.9f;
    }
}
