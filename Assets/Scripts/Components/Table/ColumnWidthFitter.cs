﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class ColumnWidthFitter : MonoBehaviour
{
    public RectTransform headerColumn;
    protected RectTransform rectTransform;
    // Start is called before the first frame update
    void Start()
    {
        this.rectTransform = this.GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        this.rectTransform.sizeDelta = headerColumn.sizeDelta;
    }
}
