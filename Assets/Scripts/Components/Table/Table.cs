﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Table : MonoBehaviour
{
    [Header("General")]
    public bool Multiselect = false;

    [Header("Colors")]
    public Color OddRowColor = Color.clear;
    public Color EvenRowColor = Color.clear;
    public Color SelectedRowColor = Color.clear;

    List<Row> rows = new List<Row>();
    List<Row> selectedRows = new List<Row>();
    void Awake()
    {
        rows = this.GetComponentsInChildren<Row>().ToList();
        ColorRows();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void FilterRows(string searchText)
    {
        foreach (var row in rows) 
        {
            row.FilterByText(searchText);
        }
        ColorRows();
    }

    protected void ColorRows()
    {
        int activeRowCounter = 1;

        foreach (var row in rows)
        {
            if (!row.isActiveAndEnabled) continue;

            if (activeRowCounter % 2 == 0)
            {
                row.Background.color = EvenRowColor;
            }
            else
            {
                row.Background.color = OddRowColor;
            }
            if (row.IsSelected)
            {
                row.Background.color = SelectedRowColor;
            }

            activeRowCounter++;
        }
    }

    public void SelectRow(Row row)
    {
        if (!Multiselect)
        {
            foreach (var selectedRow in selectedRows)
            {
                selectedRow.SetSelected(false);
            }
            selectedRows.Clear();
        }
        selectedRows.Add(row);
        row.SetSelected(true);
        ColorRows();
    }
}
