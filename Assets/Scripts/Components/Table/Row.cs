﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Row : MonoBehaviour
{
    public Image Background;
    public bool IsSelected = false;
    public List<Text> textColumns = new List<Text>();
    public GameObject checkmark;

    void Start()
    {
        textColumns = GetComponentsInChildren<Text>().ToList();
    }

    public void FilterByText(string searchText)
    {
        gameObject.SetActive(true);
        if (string.IsNullOrEmpty(searchText)) return;

        foreach (var textColumn in textColumns)
        {
            if (textColumn.text.ToLower().Contains(searchText.ToLower()))
            {
                return;
            }
        }
        gameObject.SetActive(false);
    }

    public void SetSelected(bool selected)
    {
        IsSelected = selected;
        if (checkmark != null)
        {
            checkmark.GetComponent<Image>().enabled = selected;
        }
    }

    public Text GetColumn(string key)
    {
        return transform.Find(key)?.GetComponentInChildren<Text>();
    }
}
