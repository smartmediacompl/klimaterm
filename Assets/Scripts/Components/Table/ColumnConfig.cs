﻿using System;

[Serializable]
public class ColumnConfig
{
    public string key, label;
    public int width;
    public ColumnType type = ColumnType.Text;

    public enum ColumnType
    {
        Text,
        Index,
        Checkbox,
        Options
    }
}
