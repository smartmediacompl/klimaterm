﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Incrementor : MonoBehaviour, IPointerExitHandler
{
    private Text text;
    private int innerValue;

    public void SetText(Text text)
    {
        Debug.Log("moving incrementor");
        this.text = text;
        int.TryParse(text.text, out innerValue);

        transform.position = text.transform.position;
        
        ((RectTransform)transform).sizeDelta = new Vector2(
            ((RectTransform)text.transform).rect.width + 66,
            ((RectTransform)text.transform).rect.height
            );

        gameObject.SetActive(true);
    }

    public void OnPointerExit(PointerEventData e)
    {
        gameObject.SetActive(false);
    }

    public void Increase()
    {
        innerValue++;
        text.text = innerValue + "";
    }

    public void Decrease()
    {
        innerValue = Mathf.Max(innerValue-1, 1);
        text.text = innerValue + "";
    }
}
