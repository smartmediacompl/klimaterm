﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TabPanel : MonoBehaviour
{
    public Tab ActiveTab;

    protected List<Tab> tabs;
    void Awake()
    {
        tabs = GetComponentsInChildren<Tab>().ToList();
    }

    private void OnEnable()
    {
        if (ActiveTab == null)
        {
            SetTabAsActive(tabs.FirstOrDefault());
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetTabAsActive(Tab tab)
    {
        ActiveTab?.HideContent();
        ActiveTab = tab;
        ActiveTab?.ShowContent();
    }
}
