﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tab : MonoBehaviour
{
    public TabContent TabContent;
    public bool Active = false;
    void Awake()
    {
        if (!Active)
        {
            HideContent();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowContent()
    {
        Active = true;
        TabContent?.gameObject.SetActive(true);
    }

    public void HideContent()
    {
        Active = false;
        TabContent?.gameObject.SetActive(false);
    }
}
