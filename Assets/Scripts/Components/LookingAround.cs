﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookingAround : MonoBehaviour {
    private float t = 0;

    // Update is called once per frame
    void Update()
    {
        t += Time.deltaTime * 2;
        t %= 360;
        
        transform.rotation = Quaternion.Euler(new Vector3(Mathf.Sin(t), Mathf.Cos(t), 0) * 30);
    }
}
