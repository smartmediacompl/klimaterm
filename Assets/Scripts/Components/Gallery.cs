﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Gallery : MonoBehaviour
{
    public Texture2D[] Sprites;
    public RawImage CurrentImage;

    protected int currentImageIndex = 0;

    void Start()
    {
        if (Sprites == null || Sprites.Length == 0)
        {
            gameObject.SetActive(false);
        }
    }

    private void OnEnable()
    {
        CurrentImage.texture = Sprites.FirstOrDefault();
    }

    public void ShowNextImage()
    {
        if (++currentImageIndex >= Sprites.Length)
        {
            currentImageIndex = 0;
        }
        CurrentImage.texture = Sprites[currentImageIndex];
    }

    public void ShowPreviousImage()
    {
        if (--currentImageIndex < 0)
        {
            currentImageIndex = Sprites.Length - 1;
        }
        CurrentImage.texture = Sprites[currentImageIndex];
    }
}
