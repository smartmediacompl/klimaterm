﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DataTable : MonoBehaviour
{
    public int Size { get { return Rows != null ? Rows.Length : 0; } }

    public ColumnConfig[] headerConfig;

    public RectTransform headerTransform, contentTransform;
    public Row rowTemplate;
    public GameObject cellTemplate;
    public GameObject checkmarkTemplate;
    public Incrementor incrementor;

    [Header("Colors")]
    public Color oddRowColor;
    public Color evenRowColor;
    public Color selectedRowColor;

    [Header("Selections")]
    public bool select;
    public bool multiselect;
    
    private Row[] Rows {get; set;}
    private float cellWidthFactor;
    
    private Row editedRow;

    public void Awake()
    {
        BuildHeader();
    }

    public void BuildHeader()
    {
        foreach (Transform child in headerTransform)
        {
            Destroy(child.gameObject);
        }
        
        float cellTotalWidth = 0;
        foreach (ColumnConfig c in headerConfig)
        {
            cellTotalWidth += c.width;
        }
        cellWidthFactor = headerTransform.rect.width / cellTotalWidth;


        foreach (ColumnConfig c in headerConfig)
        {
            GameObject cell = Instantiate(cellTemplate, headerTransform);
            Text cellText = cell.GetComponentInChildren<Text>();

            cellText.fontStyle = FontStyle.Bold;
            cellText.color = Color.white;
            cellText.text = c.label;
            cell.GetComponent<RectTransform>().sizeDelta = new Vector2(c.width * cellWidthFactor, 40);
            cell.SetActive(true);
        }
    }

    public void Populate(Dictionary<string, string>[] data)
    {
        foreach (Transform child in contentTransform)
        {
            Destroy(child.gameObject);
        }

        if (data == null)
        {
            Rows = new Row[0];
            return;
        }

        var rowIndex = 1;
        foreach (Dictionary<string, string> datum in data)
        {
            AddRow(datum, rowIndex);
            rowIndex++;
        }

        Rows = contentTransform.GetComponentsInChildren<Row>();
        RecolorRows();
    }

    public void AddRow(Dictionary<string, string> datum, int rowIndex)
    {
        if (rowIndex == 1)
        {
            BuildHeader();
        }

        Row row = Instantiate(rowTemplate, contentTransform);

        row.Background.color = rowIndex % 2 == 0 ? evenRowColor : oddRowColor;

        foreach (ColumnConfig col in headerConfig)
        {
            GameObject cell = Instantiate(cellTemplate, row.transform);
            cell.GetComponent<RectTransform>().sizeDelta = new Vector2(col.width * cellWidthFactor, 40);

            switch (col.type)
            {
                case ColumnConfig.ColumnType.Text:
                    cell.name = col.key;
                    cell.GetComponentInChildren<Text>().text = datum.ContainsKey(col.key) ? datum[col.key] : "";

                    if (col.key == "qty")
                    {
                        cell.AddComponent<IncrementableField>().incrementor = incrementor;
                    }

                    break;
                case ColumnConfig.ColumnType.Index:
                    cell.GetComponentInChildren<Text>().text = rowIndex + "";
                    break;
                case ColumnConfig.ColumnType.Checkbox:
                    row.checkmark = Instantiate(checkmarkTemplate, cell.transform);
                    row.checkmark.gameObject.SetActive(true);
                    break;
                case ColumnConfig.ColumnType.Options:
                    Debug.Log("setting up buttons");
                    cell.name = "menu";

                    GameObject menu = Instantiate(checkmarkTemplate, cell.transform);
                    Button[] buttons = menu.GetComponentsInChildren<Button>();
                    buttons[0].onClick.AddListener(() => {
                        editedRow = row;
                    });
                    buttons[1].onClick.AddListener(() => {
                        RemoveRow(row);
                    });
                    menu.SetActive(true);
                    break;
            }

            cell.SetActive(true);
        }

        row.gameObject.SetActive(true);

        //if (Rows != null)
        //{
            Rows = contentTransform.GetComponentsInChildren<Row>();
        //}
    }

    public void RecolorRows()
    {
        int rowCount = 0;

        foreach (Row row in Rows)
        {
            rowCount++;
            row.Background.color = row.IsSelected ? selectedRowColor : (rowCount % 2 == 0 ? evenRowColor : oddRowColor);
        }
    }

    public void SelectRow(Row row)
    {
        if (!select)
        {
            return;
        }

        if (!multiselect && !row.IsSelected)
        {
            foreach (Row selectedRow in Rows)
            {
                selectedRow.SetSelected(false);
            }
        }

        row.SetSelected(!row.IsSelected);
        RecolorRows();
    }

    public void FilterRows(string searchText)
    {
        foreach (Row row in Rows)
        {
            row.FilterByText(searchText);
        }
        RecolorRows();
    }

    public List<Row> GetSelected()
    {
        if (!select)
        {
            return new List<Row>(Rows);
        }

        List<Row> ret = new List<Row>();

        foreach (Row row in Rows)
        {
            if (row.IsSelected)
            {
                ret.Add(row);
            }
        }

        return ret;
    }

    public Dictionary<string, string> ToData(Row row)
    {
        Dictionary<string, string> newRow = new Dictionary<string, string>();

        for (int i = 0; i < headerConfig.Length; i++)
        {
            newRow.Add(headerConfig[i].key, row.textColumns[i].text);
        }

        return newRow;
    }

    public void RemoveRow(Row row)
    {
        Text qtyText = row.GetColumn("qty");
        if (qtyText != null)
        {
            int qty;
            if (int.TryParse(qtyText.text, out qty) && qty > 1)
            {
                qtyText.text = qty - 1 + "";
                return;
            }
        }

        row.gameObject.SetActive(false);
        Destroy(row.gameObject);
        Rows = contentTransform.GetComponentsInChildren<Row>(false);
        RecolorRows();
    }

    public void IncrementRow(Dictionary<string, string> data)
    {
        if (Rows == null)
        {
            AddRow(data, contentTransform.childCount + 1);
            return;
        }


        if (editedRow != null)
        {
            foreach (KeyValuePair<string, string> pair in data)
            {
                editedRow.GetColumn(pair.Key).text = pair.Value;
            }
            editedRow = null;
            return;
        }


        Row rowFound;

        foreach (Row row in Rows)
        {
            rowFound = row;

            foreach (ColumnConfig col in headerConfig)
            {
                if (col.key == "qty" || col.type != ColumnConfig.ColumnType.Text)
                {
                    continue;
                }

                Text column = row.GetColumn(col.key);

                if (column == null || column.text != data[col.key])
                {
                    rowFound = null;
                    break;
                }
            }

            if (rowFound != null)
            {
                Text qtyText = row.GetColumn("qty");
                if (qtyText != null)
                {
                    int qty;
                    if (int.TryParse(qtyText.text, out qty))
                    {
                        qtyText.text = qty + (data.ContainsKey("qty") ? int.Parse(data["qty"]) : 1) + "";
                        return;
                    }
                }

                return;
            }

        }
        
        AddRow(data, contentTransform.childCount + 1);
    }
}
