﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class IncrementableField : MonoBehaviour, IPointerEnterHandler
{
    public Incrementor incrementor;

    public void OnPointerEnter(PointerEventData e)
    {
        incrementor.SetText(gameObject.GetComponentInChildren<Text>());
    }
}
