﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.XR.ARFoundation;

public class ChoosePlane : MonoBehaviour
{
    public ARPlaneManager arPlaneManager;
    public Camera arCamera;
    private Vector2 touchPossition = default;
    private ARPlane plane = null;
    private void Awake() {
        arPlaneManager.GetComponent<ARPlaneManager>();
    }

    void Update()
    {
        if(Input.touchCount > 0){
            Touch touch = Input.GetTouch(0);

            touchPossition = touch.position;

            if(touch.phase == TouchPhase.Began){
                Ray ray = arCamera.ScreenPointToRay(touch.position);
                RaycastHit hitObject;
                if(Physics.Raycast(ray, out hitObject)){
                    plane = hitObject.transform.GetComponent<ARPlane>();
                    if(plane != null){
                        foreach (ARPlane item in arPlaneManager.trackables)
                        {
                            if(item != plane)
                                item.gameObject.SetActive(false);
                        }
                        if(plane.transform.rotation.x != 0){

                            plane.transform.localScale = new Vector3(
                                15,
                                plane.transform.localScale.y,
                                15
                            );
                        }
                    }
                }
            }
        }
        
    }
    public void PlanesCount(){
        SceneManager.LoadScene( SceneManager.GetActiveScene().name );
    }
    public void RemoveChoosedPlane(){
        if(plane != null){
            foreach (ARPlane item in arPlaneManager.trackables)
            {
                if(item != plane)
                    item.gameObject.SetActive(true);
            }
            plane.transform.localScale = new Vector3(
                1,
                plane.transform.localScale.y,
                1
            );
            plane = null;
        }
    }
}
