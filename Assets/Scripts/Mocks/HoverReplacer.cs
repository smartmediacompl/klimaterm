﻿using UnityEngine;
using UnityEngine.EventSystems;

public class HoverReplacer : MonoBehaviour, IPointerEnterHandler, IPointerUpHandler, IPointerExitHandler
{
    public GameObject normalObject, hoverObject;

    public void OnPointerEnter(PointerEventData eventData)
    {
        normalObject.SetActive(false);
        hoverObject.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        normalObject.SetActive(true);
        hoverObject.SetActive(false);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        normalObject.SetActive(true);
        hoverObject.SetActive(false);
    }
}
