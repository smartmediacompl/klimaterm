﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugCameraNormalizer : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

#if WINDOWS_UWP
        var views = FindObjectsOfType<ApplicationView>();
        foreach(var view in views)
        {
            view.transform.localScale = view.transform.localScale / 5 ;
        }
#endif
    }
}
