﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class DebugText : MonoBehaviour
{
    public bool showStackTrace;
    List<string> logs = new List<string>();
    Text text;

    void Start()
    {
        text = GetComponentInChildren<Text>();
        Application.logMessageReceived += Application_logMessageReceived;
    }

    private void Application_logMessageReceived(string condition, string stackTrace, LogType type)
    {
        logs.Add((logs.Count + 1) + ". " + condition + (showStackTrace ? "\r\n" + stackTrace : ""));
        text.text = string.Join("\r\n", logs.Reverse<string>().Take(15).ToList());
    }

    public void DebugLog(string text)
    {
        this.text.text = text;
    }
}
