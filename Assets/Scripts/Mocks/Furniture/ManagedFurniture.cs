﻿using UnityEngine;

public class ManagedFurniture : MonoBehaviour
{
    //public static StreamUploader stream;
    //private static long instanceCounter = 0;

    public long id;
    public string model;

    public bool isLightable = false;
    public bool IsOn { get; private set; } = true;

    private void Awake()
    {
        isLightable =
            (GetComponent<Renderer>() && GetComponent<Renderer>().material.IsKeywordEnabled("_EMISSION"))
            || GetComponentInChildren<Light>();

        if (isLightable)
        {
            TurnOff();
        }

        gameObject.AddComponent<Outline>().enabled = false;
    }

   /* private void Update()
    {
        UpdatePosition();
    }

    // Update is called once per frame
    public void UpdatePosition()
    {
        if (id == 0)
        {
            id = instanceCounter++;
            stream?.SendObject(this);
        }
        else
        {
            stream?.SendObjectUpdate(this);
        }
    }

    void OnDestroy()
    {
        stream?.SendRemoveObject(this);
    }*/


    public void TurnOn()
    {
        GetComponent<Renderer>().material.EnableKeyword("_EMISSION");
        foreach (Light l in GetComponentsInChildren<Light>())
        {
            l.enabled = true;
        }

        IsOn = true;
    }

    public void TurnOff()
    {
        GetComponent<Renderer>().material.DisableKeyword("_EMISSION");
        foreach (Light l in GetComponentsInChildren<Light>())
        {
            l.enabled = false;
        }

        IsOn = false;
    }
}
