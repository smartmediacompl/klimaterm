﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Unity​Engine.XR.ARFoundation;

public class ObjectPlacer : MonoBehaviour
{
    public ARAnchorManager pointManager;
    //private GestureRecognizer gesture;

    public Transform pseudoWall;

    public Rigidbody piece;

    public GameObject resetButton;
    public bool hideOnPlacement = true;

    public Text hoverText;
    public GameObject hoverWindow;

    public Button lightButton, placeButton;
    public ARPlaneManager aRPlaneManager;
    public PlaneToggle planeToggle;
    private AirConditionerController airConditionerController;

    private ManagedFurniture hoveredPiece;
    private void Awake() {
        airConditionerController = piece.GetComponent<AirConditionerController>();
        planeToggle.ToggleVisiblity(true);
    }

    public void StartPlacement(GameObject piece)
    {
        // Debug.Log("Picked up" + piece.name);
        if (this.piece != null && this.piece.gameObject != piece)
        {
            CancelPlacement();
        }

        hoveredPiece = null;
        piece.transform.parent = null;

        Outline outline = piece.GetComponent<Outline>();
        if (outline != null)
        {
            outline.enabled = false;
        }

        piece.transform.SetLayerRecursively(LayerMask.NameToLayer("Ignore Raycast"));

        Rigidbody rb = (piece.GetComponent<Rigidbody>() ?? piece.AddComponent<Rigidbody>());

        rb.position = Camera.main.transform.position;
        rb.isKinematic = true;
        rb.collisionDetectionMode = CollisionDetectionMode.Discrete;

        if (hideOnPlacement)
            {
                resetButton.SetActive(false);
                foreach (ARPlane item in aRPlaneManager.trackables)
                    item.gameObject.SetActive(true);
                if(airConditionerController != null) airConditionerController.SetOff();
                else Debug.LogError("AirConditional has not attached AirConditinalController");
            }

        this.piece = rb;
    }

    public void CancelPlacement()
    {
        if (piece != null)
        {
            Destroy(piece.gameObject);
        }
    }

    void Start()
    {
        if (piece != null)
        {
            StartPlacement(piece.gameObject);
        }
        /*GestureRecognizer gesture = new GestureRecognizer();
        gesture.SetRecognizableGestures(GestureSettings.Tap);
        gesture.Tapped += (s) =>
        {
            Debug.Log("tapped");
            Debug.Log("test1");
            Debug.Log(piece != null);
            Debug.Log("test2");
            Debug.Log(canPlace);
            if (piece != null && canPlace)
            {
                Debug.Log("placing");
                piece.gameObject.AddComponent<WorldAnchor>();
                piece = null;
                canPlace = false;
            }
            else if (hoveredPiece != null)
            {
                Debug.Log("picking up");
                DestroyImmediate(hoveredPiece.GetComponent<WorldAnchor>());
                StartPlacement(hoveredPiece.gameObject);
            }
        };
        gesture.StartCapturingGestures();

        Debug.Log((int)gesture.GetRecognizableGestures());*/

    }

    // Update is called once per frame
    void Update()
    {
        if (Physics.Raycast(
            Camera.main.transform.position,
            Camera.main.transform.forward,
            out RaycastHit hitInfo,
            30.0f,
            Physics.DefaultRaycastLayers) && piece != null)
        {
            if (hitInfo.transform.GetComponentInParent<ARPlane>()?.alignment == UnityEngine.XR.ARSubsystems.PlaneAlignment.Vertical)
            {
                //piece.transform.forward = hitInfo.normal;
                //piece.transform.Rotate(piece.transform.forward, RotateButton.value, Space.World);
                
                pseudoWall.position = hitInfo.point;
                //pseudoWall.up = hitInfo.normal;
#if UNITY_IOS
                pseudoWall.eulerAngles = new Vector3(-90, hitInfo.transform.eulerAngles.y, 0);
#else
                pseudoWall.eulerAngles = new Vector3(-90, hitInfo.transform.eulerAngles.y - 90, 0);
#endif
                pseudoWall?.gameObject.SetActive(true);
            }
            else
            {
                //piece.transform.up = hitInfo.normal;
                //piece.transform.Rotate(piece.transform.up, RotateButton.value, Space.World);
                
                pseudoWall.position = hitInfo.point;
                pseudoWall.eulerAngles = new Vector3(-90, Camera.main.transform.eulerAngles.y, 0);
                pseudoWall?.gameObject.SetActive(true);
                
            }
        }

        if (piece != null && pseudoWall.gameObject.activeSelf)
        {
            piece.transform.rotation = pseudoWall.rotation;
            piece.transform.Rotate(-90 ,180 , 0);
            piece.transform.Rotate(piece.transform.forward, RotateButton.value, Space.World);

            Vector3 oldPos = piece.position;

            piece.position = Camera.main.transform.position;
            if (piece.SweepTest(Camera.main.transform.forward, out RaycastHit hit, 50f))
            {
                piece.position += (Camera.main.transform.forward * hit.distance);
                piece.transform.position = piece.position;
            }
            else
            {
                piece.position = oldPos;
            }
        }


        //handle targeting
        Outline outline = hoveredPiece?.GetComponent<Outline>();
        if (outline != null)
        {
            outline.enabled = false;
        }
        
        if (piece == null && (hoveredPiece = hitInfo.transform?.GetComponentInParent<ManagedFurniture>()) != null)
        {
            Outline outline2 = hoveredPiece.GetComponent<Outline>();
            if (outline2 != null)
            {
                outline2.enabled = true;
            }

            lightButton.interactable = hoveredPiece.isLightable;

            hoverText.text = hoveredPiece.name;
            hoverWindow.SetActive(true);
        }
        else
        {
            lightButton.interactable = false;
            hoverWindow.SetActive(false);
        }
        
        placeButton.gameObject.SetActive(piece != null || hoveredPiece != null);
        /*if (HasTapped())
        {
            Interact();
        }*/
    }

    public void Interact()
    {
        if (piece != null)
        {
            ARAnchor refer = hoveredPiece?.GetComponentInParent<ARAnchor>()
                ?? pointManager.AddAnchor(new Pose(piece.position, piece.rotation));

            piece.transform.SetParent(refer.transform, true);
            piece.transform.SetLayerRecursively(LayerMask.NameToLayer("Default"));

            piece = null;

            if (hideOnPlacement)
            {
                resetButton.SetActive(true);
                pseudoWall.gameObject.SetActive(false);
                transform.parent.gameObject.SetActive(false);
                foreach (ARPlane item in aRPlaneManager.trackables)
                    item.gameObject.SetActive(false);
                if(airConditionerController != null) airConditionerController.SetOn();
                else Debug.LogError("AirConditional has not attached AirConditinalController");
            }
        }
        else if (hoveredPiece != null)
        {
            ARAnchor anchor = hoveredPiece.GetComponentInParent<ARAnchor>();

            StartPlacement(hoveredPiece.gameObject);

            if (anchor.transform.childCount == 0)
            {
                pointManager.RemoveAnchor(anchor);
            }
        }
    }

    public static bool HasTapped()
    {
        if (Input.touchCount == 0)
        {
            return Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject();
        }

        foreach (Touch touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began
                && !EventSystem.current.IsPointerOverGameObject(touch.fingerId))
            {
                return true;
            }
        }

        return false;
    }

    public void SwitchLight()
    {
        if (hoveredPiece != null && hoveredPiece.isLightable)
        {
            if (hoveredPiece.IsOn)
            {
                hoveredPiece.TurnOff();
            }
            else
            {
                hoveredPiece.TurnOn();
            }
        }
    }
}
