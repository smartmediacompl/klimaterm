﻿using UnityEngine;

public class PlaneToggle : MonoBehaviour
{
    public Material planeMaterial;
    public Material planeMaterial2;
    public float normalAlpha;
    public float normalAlpha2;

    public void Start()
    {
        normalAlpha = planeMaterial.color.a;
        normalAlpha2 = planeMaterial2.GetColor("_TintColor").a;
    }

    public void ToggleVisiblity(bool on)
    {
        Debug.LogError(on);
        Color color = planeMaterial.color;
        color.a = on ? normalAlpha : 0f;

        planeMaterial.color = color;

        Color color2 = planeMaterial2.GetColor("_TintColor");
        color2.a = on ? normalAlpha2 : 0f;

        planeMaterial2.SetColor("_TintColor", color);
    }
}
