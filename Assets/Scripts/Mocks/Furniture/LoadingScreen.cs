﻿using UnityEngine;
using Unity​Engine.XR.ARFoundation;

public class LoadingScreen : MonoBehaviour
{
    public GameObject[] nextScreens;

    // Update is called once per frame
    void Update()
    {
        if (Physics.Raycast(
            Camera.main.transform.position,
            Camera.main.transform.forward,
            out RaycastHit hitInfo,
            30.0f,
            Physics.DefaultRaycastLayers) && hitInfo.transform.GetComponentInParent<ARPlane>() != null)
        {
            gameObject.SetActive(false);

            foreach (GameObject go in nextScreens)
            {
                go.SetActive(true);
            }
        }
    }
}
