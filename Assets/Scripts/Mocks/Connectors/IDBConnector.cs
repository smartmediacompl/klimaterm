﻿using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class IDBConnector : MonoBehaviour
{
    public abstract void GetData(string url, Action<Dictionary<string, string>[]> callback);

    public abstract void LoadImage(string url, Action<Texture2D> callback);

    public abstract void LoadImageList(string[] url, Action<Texture2D[]> callback);

    public abstract void LoadAsset(string bundle, string model, Action<GameObject> callback);
}
