﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MockConnector : IDBConnector
{
    public override void GetData(string url, Action<Dictionary<string, string>[]> callback)
    {
        var models = Resources.LoadAll("Models/Furniture");

        Dictionary<string, string>[] result = new Dictionary<string, string>[models.Length];

        for(int i = 0; i < models.Length; i++)
        {
            Dictionary<string, string> item = new Dictionary<string, string>();
            item["name"] = models[i].name.Replace("RFAIPP", "").Replace("_"," ");
            item["img"] = "Icons/Furniture/" + models[i].name;
            item["model"] = "Furniture/" + models[i].name;

            result[i] = item;
        }




        /*Dictionary<string, string>[] result = new Dictionary<string, string>[5] {
           new Dictionary<string, string> {{"name", "Szafa" }, {"img", "" }, {"model", "Cube" } },
           new Dictionary<string, string> {{"name", "Połka" }, {"img", "" }, {"model", "OptimizedTree" } },
           new Dictionary<string, string> {{"name", "Kanapa" }, {"img", "" }, {"model", "Sphere" } },
           new Dictionary<string, string> {{"name", "Sofa" }, {"img", "" }, {"model", "SportCar20" } },
           new Dictionary<string, string> {{"name", "Biurko" }, {"img", "" }, {"model", "Conditioner" } }
        };*/

        callback(result);
    }

    public override void LoadAsset(string bundle, string model, Action<GameObject> callback)
    {
        GameObject prefab = Resources.Load<GameObject>("Models/" + model);
        callback(prefab);
    }

    public override void LoadImage(string url, Action<Texture2D> callback)
    {
        Texture2D tx = Resources.Load<Texture2D>(url);
        callback(tx);
    }

    public override void LoadImageList(string[] url, Action<Texture2D[]> callback)
    {
        callback(null);
    }
}
