﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class RotateButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
{
    public float pressSpeed = 45;
    public float clickSpeed = 45;
    public float touchDelay = 0.5f;
    public static float value = 0;

    private bool held;
    private float timer;

    public void Update()
    {
       if (held)
       {
            timer += Time.deltaTime;
            if (timer > touchDelay)
            {
                value += pressSpeed * Time.deltaTime;
                value %= 360;
            }
       }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        timer = 0;
        held = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        held = false;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        held = false;
        if (timer <= touchDelay)
        {
            value = (int)Math.Round(value / clickSpeed, MidpointRounding.AwayFromZero) * clickSpeed + clickSpeed;
            value %= 360; 
        }
    }
}
