﻿using UnityEngine;

public class MenuCaller : MonoBehaviour
{
    public GameObject menu;

    public bool automatic;
    public int followAngle = 30;

    public float minDistance = 0.5f;
    public float maxDistance = 2f;

    private Vector3 targetPosition;
    private Quaternion targetRotation;

    private float progress = 1f;

    public void Start()
    {
        SummonMenu();
    }

    public void Update()
    {
        if ((Input.GetButton("Submit") || Input.GetMouseButton(1)) && menu.activeSelf == false)
        {
            SummonMenu();
        }

        if (
            Vector3.Angle(Camera.main.transform.forward, menu.transform.position - Camera.main.transform.position) > followAngle
            || Vector3.Distance(Camera.main.transform.position, menu.transform.position) > maxDistance
            || Vector3.Distance(Camera.main.transform.position, menu.transform.position) < minDistance
            )
        {
            if (automatic)
            {
                SummonMenu();
            }
            else
            {
                menu.SetActive(false);
            }
        }

    }

    public void LateUpdate()
    {
        if (progress < 1)
        {
            progress += Time.deltaTime;
            menu.transform.position = Vector3.Lerp(
                menu.transform.position, 
                Camera.main.transform.position + Camera.main.transform.forward, progress);
            menu.transform.rotation = Quaternion.Lerp(
                menu.transform.rotation,
                Camera.main.transform.rotation, progress);

            /*progress += Time.deltaTime * 2;
            menu.transform.position = Vector3.Lerp(
                targetPosition,
                Camera.main.transform.position + Camera.main.transform.forward, progress);
            menu.transform.rotation = Quaternion.Lerp(
                targetRotation,
                Camera.main.transform.rotation, progress);*/
        }


        //menu.transform.position = Vector3.MoveTowards(menu.transform.position, targetPosition, Time.deltaTime * 5f);
        //menu.transform.rotation = Quaternion.RotateTowards(menu.transform.rotation, targetRotation, Time.deltaTime * 100f);

        //Quaternion.S
    }

    private void SummonMenu()
    {
        //menu.transform.position = Camera.main.transform.position + Camera.main.transform.forward;
        //menu.transform.rotation = Camera.main.transform.rotation;

        //targetPosition = Camera.main.transform.position + Camera.main.transform.forward;
        //targetRotation = Camera.main.transform.rotation;

        targetPosition = menu.transform.position;
        targetRotation = menu.transform.rotation;

        progress = 0;
        menu.SetActive(true);
    }
}

