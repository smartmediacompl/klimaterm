﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowHead : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //PlaceBeforeCamera(transform, 80);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0)) {
       //     PlaceBeforeCamera(200);
        }
    }

    public static void PlaceBeforeCamera(Transform obj, float dist)
    {
        obj.position = Camera.current.transform.position + (Camera.current.transform.forward * dist);
        obj.rotation = Camera.current.transform.rotation;
    }
}
