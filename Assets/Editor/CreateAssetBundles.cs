﻿using UnityEditor;
using System.IO;

public class CreateAssetBundles
{
    [MenuItem("Assets/Build AssetBundles")]
    static void BuildAllAssetBundles()
    {
        string assetBundleDirectory = "Assets/AssetBundles";
        string hololensSubdir = "/Hololens";
        string androidSubdir = "/Android";
        string iosSubdir = "/iOS";

        if (!Directory.Exists(assetBundleDirectory))
        {
            Directory.CreateDirectory(assetBundleDirectory);
        }
        if (!Directory.Exists(assetBundleDirectory + hololensSubdir))
        {
            Directory.CreateDirectory(assetBundleDirectory + hololensSubdir);
        }
        if (!Directory.Exists(assetBundleDirectory + androidSubdir))
        {
            Directory.CreateDirectory(assetBundleDirectory + androidSubdir);
        }
        if (!Directory.Exists(assetBundleDirectory + iosSubdir))
        {
            Directory.CreateDirectory(assetBundleDirectory + iosSubdir);
        }



        //BuildPipeline.BuildAssetBundles(assetBundleDirectory, BuildAssetBundleOptions.None, EditorUserBuildSettings.activeBuildTarget);
        BuildPipeline.BuildAssetBundles(assetBundleDirectory + androidSubdir, BuildAssetBundleOptions.None, BuildTarget.Android);
        BuildPipeline.BuildAssetBundles(assetBundleDirectory + iosSubdir, BuildAssetBundleOptions.None, BuildTarget.iOS);
        BuildPipeline.BuildAssetBundles(assetBundleDirectory + hololensSubdir, BuildAssetBundleOptions.None, BuildTarget.WSAPlayer);
    }
}