﻿using UnityEditor;
using UnityEngine;
using System.IO;

public class CreateModelIcons
{
    [MenuItem("Assets/Create Model Icons")]
    static void BuildIcons()
    {
        string assetPath = "Models/Furniture";
        string targetPath = Application.dataPath + "/Resources/Icons/Furniture/";

        var models = Resources.LoadAll(assetPath);
       
        for (int i = 0; i < models.Length; i++)
        {
            Texture2D baseIcon = AssetPreview.GetAssetPreview(models[i]);
            while (baseIcon == null)
            {
                baseIcon = AssetPreview.GetAssetPreview(models[i]);
                System.Threading.Thread.Sleep(80);
            }

            /*Texture2D newIcon = new Texture2D(baseIcon.width, baseIcon.height, baseIcon.format, false);


            RenderTexture tmp = RenderTexture.GetTemporary(
                    baseIcon.width,
                    baseIcon.height,
                    0,
                    RenderTextureFormat.Default,
                    RenderTextureReadWrite.Linear);
            Graphics.Blit(baseIcon, tmp);
            RenderTexture previous = RenderTexture.active;
            RenderTexture.active = tmp;

            newIcon.ReadPixels(new Rect(0, 0, tmp.width, tmp.height), 0, 0);
            newIcon.Apply();

            RenderTexture.active = previous;
            RenderTexture.ReleaseTemporary(tmp);*/

            baseIcon.mipMapBias = -1.5f;
            baseIcon.Apply();
            File.WriteAllBytes(targetPath + models[i].name + ".png", baseIcon.EncodeToPNG());

            //AssetDatabase.CreateAsset(newIcon, targetPath + models[i].name + ".png");
        }
    }
}